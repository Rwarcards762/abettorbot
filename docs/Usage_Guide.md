# Usage Guide for AbettorBot (AB3-Slash)

This page exists to provide usage guidance for all of AbettorBot's commands.

***

## Utility Commands

#### Set a reminder
> This command needs to be bug tested further.

Usage: `/reminder message (days) (hours) (minutes) (seconds)`

Arguments:

- `message`: Message to be sent to the user when the reminder completes
- `days`: Optional, number of days to set reminder for
- `hours`: Optional, number of hours to set reminder for
- `minutes`: Optional, number of minutes to set reminder for
- `seconds`: Optional, number of seconds to set reminder for

*Note: At least one time value must be used. A reminder cannot be set when no time values are given, even if all of the fields are technically optional.*

#### List all reminders
> This command needs to be bug tested further.

Usage: `/reminderlist`

#### Cancel a reminder
> This command needs to be bug tested further.

Usage: `/cancelreminder #`

Arguments:

- `#`: The number of the reminder to cancel (as shown in the reminder list)

#### See the admins and mods for the server
> This command needs to be bug tested (adding more mods/admins etc). Two bugs already resolved.

Usage: `/administratorlist`

#### View all cases (of yourself, or another user)
> This command needs to be bug tested.

*Note: You can only view cases for yourself UNLESS the ViewAllWarns option is set to* **True** *by the owner of the server.* *Administrators/Moderators can see all cases.*

Usage: `/cases (@user)`

Arguments:

- `@user`: Optional, a user to view their cases

#### View a particular case (ban/kick/etc.)
> This command needs to be bug tested.

*Note: You can only view cases for yourself UNLESS the ViewAllWarns option is set to* **True** *by the owner of the server.* *Administrators/Moderators can see all cases.*

Usage: `/viewcase number`

Arguments:

- `number`: Case number to view

***

## Standard User Color Commands

#### Generate a random color

Usage: `/abcolors randomcolor`

***

## Managing Administrative Users Commands

#### Check the permissions level of a user
> This command needs to be bug-tested.

Usage: `/perms (@user)`

Arguments:

- `@user`: Optional, a user to check their permissions (without a mention, will return your permissions)

#### Add an administrator for the server
> This command needs to be bug-tested. 
> Usable by the owner of a server, or Admins if "AdmAddAdm" is set to "True".

Usage: `/addadmin @user`

Arguments:

- `@user`: The user to make an administrator


#### Remove an administrator for the server
> This command needs to be bug-tested.
> Usable by the owner of a server, or Admins if "AdmAddAdm" is set to "True".

Usage: `/deladmin @user`

Arguments:

- `@user`: The user to remove from being an administrator, cannot be the Owner of the server

#### Add a moderator for the server
> This command needs to be bug-tested.
> Usable by the owner of a server, or Admins if "AdmAddMod" is set to "True".

Usage: `/addmod @user`

Arguments:

- `@user`: The user to make a moderator

#### Remove a moderator for the server
> This command needs to be bug-tested.
> Usable by the owner of a server, or Admins if "AdmAddMod" is set to "True".

Usage: `/delmod @user`

Arguments:

- `@user`: The user to remove from being a moderator

#### View currently enabled moderator commands
> This command needs to be bug tested.

Usage: `/modcommands list`

#### Add a command to the approved moderator command list
> This command needs to be bug tested.
> Usable by the owner of the server, or Admins if "AdmEditModCom" is set to True.

Usage: `/modcommands enable option`

Arguments:

- `option`: A command from the list provided to enable

#### Remove a command from the approved moderator command list
> This command needs to be bug tested.
> Usable by the owner of the server, or Admins if "AdmEditModCom" is set to True.

Usage: `/modcommands disable option`

Arguments:

- `option`: A command from the list provided to disable

***

## Administrative Commands
> Commands in this section are usable by Administrators. They are also usable by Moderators (if added to approved command list.)

#### Deleting messages in bulk or from users
> This command needs to be bug tested.

Usage: `/prune number_of_messages (@user)`

Arguments:

- `number_of_messages`: Number of messages to delete (if no user provided), or number of messages to scan through (if user provided)
- `@user`: Optional, specific user's messages to delete

#### Mute / time-out a user
> This command needs to be bug tested.

*Note: All time arguments are optional, however at least ONE time value must be given or else the command will not work (obviously.)*

Usage: `/mute @user (reason) (days) (hours) (minutes) (seconds)`

Arguments:

- `@user`: User to time out
- `reason`: Optional, a reason for muting the user (defaults to "No reason provided.")
- `days`: Optional, number of days to mute
- `hours`: Optional, number of hours to mute
- `minutes`: Optional, number of minutes to mute
- `seconds`: Optional, number of seconds to mute

#### Unmute / un-time-out a user
> This command needs to be bug tested.

Usage: `/unmute @user (reason)`

Arguments:

- `@user`: User to un-time-out
- `reason`: Optional, a reason for unmuting the user (defaults to "No reason provided.")

#### Ban a user
> This command needs to be bug tested.

Usage: `/ban @user (reason) (days)`

Arguments:

- `@user`: User to ban
- `reason`: Optional, a reason for banning the user (defaults to "No reason provided.")
- `days`: Optional, number of days worth of message history from the user to delete, between 0 and 7 days

#### Unban a user
> This command needs to be bug tested.

Usage: `/unban id_of_user (reason)`

Arguments:

- `id_of_user`: User's Discord ID to unban
- `reason`: Optional, a reason for unbanning the user (defaults to "No reason provided.")

#### Kick a user
> This command needs to be bug tested.

Usage: `/kick @user (reason)`

Arguments:

- `@user`: User to kick
- `reason`: Optional, a reason for kicking the user (defaults to "No reason provided.")

#### Warn a user
> This command needs to be bug tested.

Usage: `/warn @user severity reason ...`

Arguments:

- `@user`: User to warn
- `severity`: Severity of warning from 1 (Low) to 3 (High)
- `reason`: Optional, reason for warning (defaults to "No reason provided.")

#### Remove a warning/ban/kick case
> This command needs to be bug tested.

Usage: `/removecase case_number`

Arguments:

- `case_number`: Case number desired to be removed, will ask for confirmation

#### Remove all cases from a user
> This command needs to be bug tested.

*Note: Administrators can only use this command if the ResetUserWarn option is set to* **True** *by the owner of the server. Moderators cannot use this command.*

Usage: `/removecases @user`

Arguments:

- `@user`: User to remove cases for

#### Reset all cases in the server
> This command needs to be bug tested.

*Note: Administrators can only use this command if the ResetAllWarn option is set to* **True** *by the owner of the server. Moderators cannot use this command.*

Usage: `/removeallcases`

***

## Color System Moderation Commands

#### Enable the color system for the server

Usage: `/abcolors admins enable`

#### Disable the color system for the server

Usage: `/abcolors admins disable`

#### Enable/Disable custom names for color roles

Usage: `/abcolors admins names option`

Arguments:

- `option`: Either 'enable' or 'disable'

#### Enable/Disable ability for users to create custom color roles

Usage: `/abcolors admins create option`

Arguments:

- `option`: Either 'enable' or 'disable'

***

## Server Owner Commands

#### See the settings for the server
> This command has been bug tested (all possible inputs tested) with no issues.

Usage: `/ownerconfiguration status`

#### Change the settings for the server
> This command has been bug tested (all possible inputs tested) with no issues.

Usage: `/ownerconfiguration set option value`

Arguments:

- `key`: A corresponding key for a setting for the server, matching the settings found in `/ownerconfiguration status`
- `value`: True or False
