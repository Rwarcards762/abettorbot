# How to Delete Respond-based Messages

!!! warning
    Ephemeral messages CANNOT be deleted in this fashion, or at all.
    <br />This is because they leave the Discord context entirely -- and are up to the user to dismiss.

```py title="Snip of a command"
await ctx.respond("thing")
msgObj = await ctx.interaction.original_message()
await msgObj.delete()
```