# Server Timeout (Replacement of Legacy Muting)

Discord FINALLY added native muting abilities handled on Discord's side.

The PyCord documentation can be found [here](https://docs.pycord.dev/en/document-app-commands/api.html?highlight=timeout#discord.Member.timeout).

The Discord article discussing the concept can be found [here](https://support.discord.com/hc/en-us/articles/4413305239191-Time-Out-FAQ).

The timeout command is part of `discord.Member` and the command is as follows:

```py
await userObject.timeout(until, reason="string")
# until: a datetime.datetime object that states when the mute should END
# reason: a string to show on Audit Log, reason for mute
```

To un-timeout a `discord.Member`:

```py
await userObject.timeout(None, reason="string")
# Both of these options are equivalent commands. Setting timeout to "None" removes a timeout.
await userObject.remove_timeout(reason="string")
```

To check if a user is timed out:

```py
boolean_return = userobj.timed_out
# Will return True or False
```

