# Changes to `ctx`

* `ctx.message` is no longer valid or in-scope. Discord bots no longer utilize this without a SPECIALLY GRANTED permission.
    * Therefore, all calls that used this such as `ctx.message.guild.owner_id` now rely on the `ctx` itself, ie `ctx.guild.owner_id`.
