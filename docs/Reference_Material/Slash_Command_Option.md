# Slash Commands: Using the `Option()` Argument

!!! quote "References"
    [Discussion on Pycord](https://github.com/Pycord-Development/pycord/discussions/687)
    <br />[Pycord Example](https://github.com/Pycord-Development/pycord/blob/master/examples/app_commands/slash_options.py)

For slash commands, arguments can be specified using `Option()`.

For example:

```py title="Example Usage"
@slash_command(name="perms", description="Check permissions of a user for AbettorBot", guild_ids=[xxxyyyzzz])
async def perms(self, ctx, user: Option(discord.User, "User to check permissions for", required=False)):
    ...
    # Attributes can then be called, as it passes a real Discord User object!
```

The definitive list currently exists [here on PyCord](https://docs.pycord.dev/en/document-app-commands/api.html#discord.SlashCommandOptionType) and [here on Discord's documentation](https://discord.com/developers/docs/interactions/application-commands#application-command-object-application-command-option-type).

| Name | Value | Additional Notes |
| - | - | - |
| SUB_COMMAND |	1 | |
| SUB_COMMAND_GROUP | 2 | |	
| STRING | 3 or `str` | |
| INTEGER | 4 or `int` | Any integer between -2^53 and 2^53 |
| BOOLEAN | 5 | |
| USER | 6 or `discord.User` | Takes in Discord mentions, user#discrim, or discord IDs |	
| CHANNEL | 7 | Includes all channel types + categories |
| ROLE | 8 | |	
| MENTIONABLE | 9 | Includes users and roles |
| NUMBER | 10 or `float` | Any double between -2^53 and 2^53 |

To use the numbers specifically (1, 2, ...), must import from `discord.enums`.