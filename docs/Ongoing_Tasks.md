# Ongoing Tasks

This page exists to provide a roadmap for preparing AbettorBot 3 (AB3-Slash) to be released.

This page also gives insight into some of the backend of AbettorBot, which will be released as Open Source on GitLab upon its release.

## Long-Standing Goals

- [ ] Remove all legacy functions that can be replaced with built-in functionality
    - [ ] Remove all "getuserIDfromMention" usage, replace with requiring `discord.User` option as argument, etc.
- [ ] Ensure there is not an easier way to track reminders :)
- [ ] Migrate "commonly used functions" like `admincheck` to the `ab_utils.py` file, and import them as needed (IN PROGRESS)
    - [x] admincheck
    - [x] mkdir_p
    - [x] "Confirm" / "Cancel" buttons view class as "Confirm()"
        - stores values into viewObj.value as True, False, or None for timeout
    - [x] badMessage (originally AtHereEveryone), returns True if message has `@here` or `@everyone`
    - [x] queryModConfig / queryOwnerConfig for easy retrieval of database values
        - [ ] Next up: queryAdminList, queryModList, etc...?
    - [ ] Database writes in common function (same reason as above)?
    - [ ] More to come... list will fill as items are added
- [ ] Once commands are completed, go through and confirm errors for API calls are handled if required (such as HTTPExceptions, Forbiddens, etc.)
- [ ] Once commands are completed, go through and determine permissions needed for each command and add a `@commands.bot_has_permissions` decorator to ensure commands provide information if a permission is missing

## Main Progress Tracking

- [x] Complete development testing to ensure cog functionality remains when switching to slash-based commands
- [x] Complete development testing to ensure logging functionality can exist in cogs
    - Logging in cogs is as simple as importing `logging` and using it! It utilizes the handler from the parent!
- [x] Complete development testing to ensure `Option()` arguments function as intended and can accomplish desired goals
    - [x] [Documentation added](./Reference_Material/Slash_Command_Option.md) regarding how to use `Option()` and the types of arguments supported

??? note "Notes regarding original AB3 configuration vs AB3-Slash"
    I got really carried away with "configuration" when initially approaching AB3.
    <br />Configuration is cool, but some configuration is overkill.
    <br />The warning system should always be enabled. It's now enabled by default and set up when the very first AdminCheck is run.
    <br />Enabling and disabling mods? Just doesn't make sense. They exist by default and aren't toggle-able.
    <br />Will help remove a ton of bloat from code.

### Administrative Role Related Commands

- [x] Migration of AbettorBot3 "admin.py" to AB3-Slash (`admin.py` AdminCog, used for Role-based stuff)
    - [x] Revising all calls to `admincheck` should now not fail when returning `New`, but should instead run AGAIN (why didn't I think of this before?)
        - This will be a process over time as I revise the commands whilst rewriting them to work for slash-based interactions :)
    - [x] `perms` (originally named "admincheck")
    - [x] `ownerconfig` (rework to use new UI buttons / menus)
    - [x] `addadmin`
    - [x] `deladmin @user` -- takes in User IDs as well! Solves issue #1.
    - [x] `addmod`
    - [x] `delmod @user` -- takes in User IDs as well! Solves Issue #1.
    - ~~enablemods/disablemods~~ Removing support for this entirely, I don't think it would make any sense to have THAT level of configuration
    - [x] ~~`adminlist` / `modlist`~~ Combined into `administratorlist`, usable by anyone
    - [x] `modcommands` -- renaming from editmodcommands
        - [x] `list`: give list of active mod commands
            - [x] ~~need to determine who should be able to use this command~~ leaving as everyone can use it for now
        - [x] `enable`: enable a command for mods
        - [x] `disable`: disable a command for mods

***

### Administrative Tool Related Commands

- [x] Migration of "admin.py" and "warning.py" (`admincommands.py`, AdminCommandCog, used for actual administrative work)
    - [x] `purge`
        - [x] add feature to purge only messages from a certain member in one channel?
        - [ ] feature to purge messages from a certain user in ANY channel in the server?
        - [ ] option to prune before/after/around a certain time (eventually)
    - [x] `ban @user` 
        - [x] ~~ability to ban a user by ID (to ban users no longer in a server)~~ `discord.User` takes care of that!
    - [x] `unban ID`
    - [x] `kick`
    - ~~enable/disable warnings~~ This was a dumb idea. There's no reason to disable these. They should always be on, that way kick/mutes/etc always get cases without the "warning system" needing to be "enabled." Trying too hard.
    - `resetwarnings` -- being changed into more "case" style wording and split into two commands
        - [x] `resetcases @user`
        - [x] `resetallcases`
    - [x] `warn @user level reason ...`
    - [x] `removecase casenumber` (originally "removewarn", making name more generic to apply to all cases)
    - [x] `viewcase casenumber`
    - [x] `cases (@user)` (renamed from "warnings", since this is for ALL cases, not just warnings)
        - using new Paginator methods to switch between embeds, rather than the AB2.5 jank method with reactions
        - user to view their own warnings ephemerally, or publicly if enabled for the server
    - [x] `mute @user reason... days hours minutes seconds`
        - [x] [Documentation added](./Reference_Material/ServerTimeout.md) regarding the new "server timeout" feature
    - [x] `unmute @user reason`
        - only the unmute command creates an "unmute" record -- a 'natural' unmute through time expiration will not

***

### Reminder Commands

- [x] Migration of AbettorBot3 Reminders to AB3-Slash
    - [x] Ensure mute system, if abandoned for "server timeout" methods, is wiped from all reminder code
    - [x] Ensure reminder codebase is as clean and straightfoward as possible
        - [ ] If possible at all, fragment reminder code into a cog or separate file[^rem]
    - [x] `/reminder`
        - Reminders persist through a restart using the reminder database
        - Late reminders sent as required
    - [x] `/reminderlist`
        - [ ] possible to provide a time for reminder to complete? or stick with current time system (time remaining)?[^rem2]
    - [x] `/cancelreminder #`

[^rem]: Opting not to do this for now, may do it at some point.

[^rem2]: Focusing on functionality right now, so will possibly come back to this later.

***

### Color System Commands

- [x] Determine database plans and document them
- [ ] Create script for converting OLD color system information into new TinyDB databases
    - this is vital in preventing old colors from being "lost" when moving to AB3-Slash
- [ ] Migration of Color System from AB2 to AB3-Slash
    - User command group `abcolors`
        - [x] `/abcolors randomcolor`
        - [x] `/abcolors viewcolor hexcode`
        - [x] `/abcolors createcolor hexcode name ...`
        - [ ] `/abcolors setcolor name ...`
            - This one will be interesting... I wonder if there's an easy way to do this...
            - Maybe using the Option autocomplete feature, could query a list of all color role names in server?
        - [ ] `/abcolors listcolors`
    - Admin subcommandgroup `abcolors admin`
        - [x] `/abcolors admins enable/disable` -- enable or disable the color system, walking through the configuration
        - [x] `/abcolors admins names enable/disable` -- flag for whether or not users can custom name roles
        - [x] `/abcolors admins create enable/disable` -- flag for whether or not users can create custom colors
        - [ ] `/abcolors admins reset` -- wipe everything and start over
        - [ ] `/abcolors admins clean` -- clean out old roles not being used by anyone anymore
        - [ ] `/abcolors admins delete rolename` -- delete a specific color role

***

### Birthday System Commands

- [ ] Migration of Birthday System from AB2 to AB3-Slash
    - [ ] `/birthday view/set/clear (@user)`: view own or a user's, set your own, or clear your own
    - [ ] administrative command for setting up the happy birthday messages / roles

***

### Misc. Commands

- [ ] Migration of "Fun" commands
    - [ ] 8ball
    - etc...

***

### Utility Commands

- [ ] `/help` menu
    - based on AB2 help menu, but hopefully interactive or something
    - not sure what my plans are for this yet, and this will probably roll out as an update rather than at release
- [ ] `/ping`
- [ ] `/userinfo (@user)`
    - make sure to patch the userinfo bug!
- etc...
    
