# AbettorBot

This is the Documentation website for AbettorBot. **Please note that this site is NOT currently up to date and likely will not be until full public release.**

This site is for me, Rwarcards762, to:

- document my work on AbettorBot 3
- devise "how-to" guides for some concepts of writing a bot, or notes for myself that could prove useful to others
- build how-to guides for using AbettorBot
- provide help information regarding usage of AbettorBot

Ongoing development will be tracked in [Ongoing Tasks](./Ongoing_Tasks.md) -- this is the best place to see my current progress with AbettorBot 3, as well as what sort of concepts are planned.

The User Guide for using AbettorBot 3 will be found on [this page](./Usage_Guide.md). Note that this is for the unreleased code, so none of these are public yet!

Random notes that I've taken for myself can be found in the Reference Material section.

***

## Features

### Asynchronous Reminder System
Featuring cancellable reminders displayed in a friendly and intuitive format

![reminders.gif](./Images/reminders.gif)
