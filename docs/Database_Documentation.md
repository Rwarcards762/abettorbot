# Database Documentation for AbettorBot for AbettorBot 3 (future-v2.0.0)
This document is used to track development and changes in the backend for AbettorBot.

***
***
***

# Admin Database (./serverID/admins.json)
## Owner Configuration Entry
*Attributes found in entry with `OwnerConfig:True` keypair.* *These values can only be adjusted by the owner of a server using the `ownerconfig` command.*

> All keys either have a value of True or False in this entry.

!!! important
    `EnableDisableWarn` and `AdminMuteSystem` are being deprecated.

| Key | Value Meaning |
| ----- | ----- |
| *AdmAddAdm* | ability for admins to add/remove other admins |
| *AdmAddMod* | ability for admins to add/remove moderators |
| ~~*EnableDisableWarn*~~ | ~~ability for admins to enable/disable warning system for server~~ |
| *ResetUserWarn* | ability for admins to reset a user's warnings |
| *ResetAllWarn* | ability for admins to reset all warnings in a server |
| *AdmEditModCom* | ability for user to add/remove to/from the approved moderator command list |
| *ViewAllWarns* | ability for users to view other user's warnings cases |
| ~~*AdminMuteSystem*~~ | ~~ability for admins to enable/disable and configure the mute system~~ |

## Moderation System Status + Command List Entry
*Found via the `ModConfig` key.*

| Key | Values | Value Meaning |
| ----- | ----- | ----- |
| *ModConfig* | "True" | used to identify the moderation command list |
| *ComList* | empty list, or list of commands | contains list of approved moderator commands, should only be accessed if `ModsEnabled:True` |

## Administrator + Moderator Entries
*Found via `"Level":"Admin"` and `"Level":"Mod"` respectively.*

| Key | Value | Value Meaning |
| ----- | ----- | ----- |
| *Level* | "Admin" or "Mod" | Entry's *UserID* describes an Admin or Mod for the server, respectively |
| *UserID* | `int` | Discord UserID for user designated as Admin or Mod |

***
***
***
# Warning Database (./serverID/warnings.json)
>Note: Entries are only created in this database (ex. kick, ban, etc.) if the Warning System is set to Enabled. Otherwise, no entries should be created in this database.

## Case Number Entry
> Valid as of AB3-Slash 2/8/22.

This entry is used to keep track of the current case number for the server. It should be iterated every time a new case is created, and reset if all cases are reset.

| Key | Value | Value Meaning |
| ----- | ----- | ----- |
| *Type* | "CaseNo" | Used for searching to easily find this entry |
| *Case* | `int` | current case number for server (next case created should use this number) |

## Enabled/Disabled Entry
??? warning "Deprecated"
    This is being entirely deprecated -- the Warning system will always be enabled.

    ~~This entry describes whether or not the Warning System is enabled for a server.~~

    | Key | Value | Value Meaning |
    | ----- | ------ | ----- |
    | *Type* | "Enabled" | Used for searching to easily find this entry |
    | *Enabled* | True/False | describes whether or not the Warning System is currently enabled |

## Warning Entries
> Valid as of AB3-Slash 2/8/22.

These entries describe warn cases for a server.

| Key | Value | Value Meaning |
| ----- | ------ | ------ |
| *Type* | "Warn" | used for searching easily to find warning entries |
| *Case* | `int` | case number |
| *UserID* | `int` | Discord User ID of person being warned |
| *Level* | 1, 2, 3 (`int`) | severity of case |
| *Contents* | `str` w/ length less than 1024 | reason for warning |
| *Timestamp* | `str` | time entry was created, should always be in format of return value of `strftime(%B %d, %Y %I:%M%p %Z")` |
| *ChannelID* | `int` | channel ID in which case was created |
| *Creator* | `int` | Discord User ID of admin/mod who created case |

## Ban Entries
> Valid as of AB3-Slash 2/8/22.

These entries describe ban cases for a server.

| Key | Value | Value Meaning |
| ----- | ------ | ----- |
| *Type* | "Ban" | used for searching easily to find ban entries |
| *Case* | `int` | case number |
| *UserID* | `int` | Discord User ID of person being banned |
| *Contents* | `str` w/ length less than 1024 | reason for ban |
| *Timestamp* | `str` | time entry was created, should always be in format of return value of `strftime(%B %d, %Y %I:%M%p %Z")` |
| *ChannelID* | `int` | channel ID in which case was created |
| *Creator* | `int` | Discord User ID of admin/mod who created case |

## Unban Entries
> Valid as of AB3-Slash 2/8/22.

These entries describe unban cases for a server.

| Key | Value | Value Meaning |
| ----- | ------ | ----- |
| *Type* | "Unban" | used for searching easily to find unban entries |
| *Case* | `int` | case number |
| *UserID* | `int` | Discord User ID of person being unbanned |
| *Contents* | `str` w/ length less than 1024 | reason for unban |
| *Timestamp* | `str` | time entry was created, should always be in format of return value of `strftime(%B %d, %Y %I:%M%p %Z")` |
| *ChannelID* | `int` | channel ID in which case was created |
| *Creator* | `int` | Discord User ID of admin/mod who created case |

## Kick Entries
> Valid as of AB3-Slash 2/8/22.

These entries describe kick cases for a server.

| Key | Value | Value Meaning |
| ----- | ------ | ----- |
| *Type* | "Kick" | used for searching easily to find kick entries |
| *Case* | `int` | case number |
| *UserID* | `int` | Discord User ID of person being kicked |
| *Contents* | `str` w/ length less than 1024 | reason for kick |
| *Timestamp* | `str` | time entry was created, should always be in format of return value of `strftime(%B %d, %Y %I:%M%p %Z")` |
| *ChannelID* | `int` | channel ID in which case was created |
| *Creator* | `int` | Discord User ID of admin/mod who created case |

## Mute Entries
> Valid as of AB3-Slash 2/8/22.

These entries describe mute cases for a server. These entries are only created if a user is timed out using AB3-Slash.

| Key | Value | Value Meaning |
| ----- | ------ | ----- |
| *Type* | "Mute" | used for searching easily to find mute entries |
| *Case* | `int` | case number |
| *UserID* | `int` | Discord User ID of person being muted |
| *Contents* | `str` w/ length less than 1024 | reason for mute |
| *Timestamp* | `str` | time entry was created, should always be in format of return value of `strftime(%B %d, %Y %I:%M%p %Z")` |
| *ChannelID* | `int` | channel ID in which case was created |
| *Creator* | `int` | Discord User ID of admin/mod who created case |
| *Length* | `str` | duration of mute in readable format (`x days y hours z minutes a seconds`) |

## Unmute Entries
> Valid as of AB3-Slash 2/16/22.

These entries describe unmute cases for a server. Only generated if an unmute was done by AB3-Slash -- will not be created for "natural" unmutes (time expiring on a timeout.)

| Key | Value | Value Meaning |
| ----- | ------ | ----- |
| *Type* | "Unmute" | used for searching easily to find unmute entries |
| *Case* | `int` | case number |
| *UserID* | `int` | Discord User ID of person being unmuted |
| *Contents* | `str` w/ length less than 1024 | reason for unmute |
| *Timestamp* | `str` | time entry was created, should always be in format of return value of `strftime(%B %d, %Y %I:%M%p %Z")` |
| *ChannelID* | `int` | channel ID in which case was created |
| *Creator* | `int` | Discord User ID of admin/mod who created case |

***
***
***

# Mute Entry Database (./serverID/mute.json)
Deprecated.

??? info "Deprecated for new 'Server Timeout' Method"
    This entire system is being deprecated thanks to new Discord timeout feature.

    > Note: This system works as a backend in tandem with the Warning Entry Database. This database is used to keep track of the logistics functions for a mute, not as a "permanent record."\
    > A permanent record of mutes should be kept in the Warning Database when servers have the Warning System enabled.

    ## Mute System Status/Configuration Entry
    This entry defines the current state of the mute system and its configuration.

    | Key | Value | Value Meaning |
    | ----- | ----- | ----- |
    | *MuteSystem* | "True" | used for searching easily to find the MuteSystem configuration entry |
    | *MuteEnabled* | True/False | defines whether or not the mute system is enabled for a server |
    | *MuteRole* | `int` | role ID of designated "mute role" in server. Should be `0` if mute system is not configured or disabled. | 

    ## Mute Entries
    > Note: These entries are still under development/testing and are not finalized or complete.\
    > For more information on the development process and flow of commands planned, please see the documentation found in the Development Documentation folder.

    These entries store logistical information for allowing mutes/unmutes to occur.

    > Each mute entry in the Mute Database generates a Task on startup of the bot, where the task name is saved in GLOBAL_TASK_ARRAY under name described below.

    | Key | Value | Value Meaning |
    | ----- | ----- | ----- |
    | *Type* | "Mute" | used for searching easily to find mute entries |
    | *UserID* | `int` | Discord User ID of person who is muted |
    | *MessageID* | `int` | messageID that mute command was called with |
    | *ChannelID* | `int` | channelID where mute command was called from |
    | *ServerID* | `int` | serverID where mute command was called |
    | *Contents* | `str` | (undetermined)
    | *CompletionTime* | `int` | time value when *mute* should expire |
    | *TimerValue* | `int` | original mute duration |
    | *TaskName* | `str` in format "mute" + str(userID) + str(messageID) | used to identify mute task running |

***
***
***
# Reminder Database (./reminders.json)
This database stores all global reminder data.

## Reminder Entries
These entries store information related to reminders.

| Key | Value | Value Meaning |
| ----- | ----- | ----- |
| *Type* | "Reminder" | used for searching easily to find reminder entries |
| *UserID* | `int` | user ID of user that set reminder |
| *MessageID* | `int` | messageID of reminder call |
| *ChannelID* | `int` | channelID where reminder was called |
| *ServerID* | `int` | serverID where reminder was created |
| *Contents* | `str` | reminder message |
| *CompletionTime* | `int` | time when user wants to be reminded |
| *TimerValue* | `int` | original length of reminder |
| *TaskName* | `str`, format of "reminder" + str(userID) + str(messageID) | used to track reminder task in `GLOBAL_TASK_ARRAY` |

![Reminder Logic Flow](./Images/AB3-ReminderGraph.png)

***
***
***
# Color System Database (./serverID/colorsystem.json)
No plans determined at this time. Will be built in a way that can be converted from old to new without data loss -- however, will feature a cleaner interface (by far haha.)

## Color System Status/Configuration Entry
This entry defines the current state of the color system and its configuration.

| Key | Value | Value Meaning |
| ----- | ----- | ----- |
| *Type* | "ColorSystem" | used for searching easily to find the ColorSystem configuration entry |
| *ColorSystemEnable* | "True"/"False" | defines whether or not the color system is enabled for a server |
| *CustomColorEnable* | "True"/"False" | defines whether or not users can create their own color roles in a server |
| *CustomNamesEnable* | "True"/"False" | defines whether or not users can set custom names for their color roles |

*Note: If `CustomColorEnable` is `True` while `CustomNamesEnable` is `False`, users will be able to create their own color roles, but will not be able to set custom names for them. The names will be the hex value of the color by default.*

## Color Role Entries
These entries store information related to color roles for a server.

| Key | Value | Value Meaning |
| ----- | ----- | ----- |
| *Type* | "ColorRole" | used for searching easily to find color role entries |
| *RoleID* | `int` | role ID of color role |
| *ColorHex* | `str` | hexcode of color role |
| *ColorName* | `str` | name of color role in server |
